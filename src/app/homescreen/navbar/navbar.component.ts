import { Component, OnInit } from '@angular/core';
import { Cookie } from 'ng2-cookies';
import { MemberService } from '../service/memberservice';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {


  isLoggedIn = Cookie.check('access_token');
  constructor(private memberService: MemberService) { }
  ngOnInit(): void {
  this.isLoggedIn = Cookie.check('access_token');
    console.log(this.isLoggedIn);
  }

  login() {
    this.memberService.redirectToLogin();
    }
  logout() {
    this.memberService.logout();
  }
}
