import { templateSourceUrl } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { MemberService } from './service/memberservice';

@Component({
  selector: 'app-homescreen',
  templateUrl: './homescreen.component.html'
})
 
export class HomescreenComponent {
  public isLoggedIn = false;

  constructor(private memberService: MemberService) { }
 
  ngOnInit() {
    this.isLoggedIn = this.memberService.checkCredentials();    
    let i = window.location.href.indexOf('code');
    if(!this.isLoggedIn && i != -1) {
      this.memberService.getToken(window.location.href.substring(i + 5));
    }
  }
 
  secure() {
    this.memberService.getSecure();
  }
  notSecure() {
    this.memberService.getNotSecure();
  }

}