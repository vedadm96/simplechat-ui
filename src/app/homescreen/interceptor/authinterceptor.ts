import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Cookie } from "ng2-cookies";
import { catchError, throwError } from "rxjs";
import { Observable } from "rxjs/internal/Observable";
import { MemberService } from "../service/memberservice";


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private memberService: MemberService) {

  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
        if (err.status === 401) {
            this.memberService.redirectToLogin();
        }
            return throwError(err);
    }));
  }
}