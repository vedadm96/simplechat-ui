import {Injectable} from '@angular/core';
import { Cookie, CookieService } from 'ng2-cookies';
import { HttpClient, HttpHeaders } from '@angular/common/http';
 

@Injectable()
export class MemberService {
  public clientId = 'member-service';
  public redirectUri = 'http://localhost:4200';

  constructor(private _http: HttpClient) { }

  saveToken(token: any) {
    var expireDate = new Date().getTime() + (1000 * token.expires_in);
    Cookie.set("access_token", token.access_token, expireDate);
    console.log('Obtained Access token');
    window.location.href=this.redirectUri;
  }

  getToken(code: string) {
    let params = new URLSearchParams();   
    params.append('grant_type','authorization_code');
    params.append('client_id', this.clientId);
    params.append('client_secret', 'newClientSecret');
    params.append('redirect_uri', this.redirectUri);
    params.append('code',code);
    let headers = 
      new HttpHeaders({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8'});
      this._http.post('http://localhost:8090/auth/realms/simplechat/protocol/openid-connect/token', 
        params.toString(), { headers: headers })
        .subscribe(
          data => this.saveToken(data),
          err => alert('Bad Credentials')); 
  }

  getSecure(){
    let headers = 
      new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8', 
    'Authorization' : ('Bearer ' + Cookie.get('access_token')) ,
    'Access-Control-Allow-Origin' : 'http://localhost:4200'});
      this._http.get('http://localhost:8080/secured',{headers:headers}).subscribe(
          data => console.log(data)
      )
  }
  getNotSecure(){
    let headers = 
      new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8', 
    'access-control-allow-origin' : 'http://localhost:4200'});
    headers.append('Access-Control-Allow-Credentials', 'true');
      this._http.get('http://localhost:8080/not-secured',{headers:headers}).subscribe(
          data => console.log(data)
      )
  }

  checkCredentials() {
    return Cookie.check('access_token');
  } 


  redirectToLogin() {
    window.location.href = 'http://localhost:8090/auth/realms/simplechat/protocol/openid-connect/auth?response_type=code&scope=openid%20read&client_id=' +this.clientId + '&redirect_uri='+ this.redirectUri;
  }

  logout() {
    console.log('USO');
    Cookie.delete('access_token');
    console.log(Cookie.check('access_token'));
    window.location.href = 'http://localhost:8090/auth/realms/simplechat/protocol/openid-connect/logout?redirect_uri='+ this.redirectUri;
  }
}